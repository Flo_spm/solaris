/*
 * Code d'exemple pour une photorésistance.
 */
#include <Servo.h>

const int SERVO_PIN = 9;
const int LDR_PIN0  = A0;
const int LDR_PIN1  = A1;
const int LDR_PIN2  = A2

;Servo myservo; // create servo object to control a servo  
int   pos = 0;    // variable to store the servo position

void deplacement_gauche(int deplacement)
{
  pos = pos + deplacement;
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(10);
}

void deplacement_droite(int deplacement)
{
  pos = pos - deplacement;
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(10);
}

int lecture_capteur(int pin)
{
  return analogRead(pin);
}

// Fonction setup(), appelée au démarrage de la carte Arduino
void setup() 
{
  // Initialise la communication avec le PC
  Serial.begin(9600);
  
// twelve servo objects can be created on most boards
  myservo.attach(SERVO_PIN);  // attaches the servo on pin 9 to the servo object

  // déplacement gauche et droite
  
}

// Fonction loop(), appelée continuellement en boucle tant que la carte Arduino est alimentée
void loop() 
{
  int valeur_gauche = lecture_capteur(LDR_PIN0);
  Serial.println("capteur gauche : " + String(valeur_gauche));
  
  int valeur_droite = lecture_capteur(LDR_PIN1);
  Serial.println("capteur droite : " + String(valeur_droite));
  
   int valeur_arriere = lecture_capteur(LDR_PIN2);
  Serial.println("capteur arriere  : " + String(valeur_arriere));
  
  
  // Envoi la mesure au PC pour affichage et attends 1000ms
  delay(1000);
}
