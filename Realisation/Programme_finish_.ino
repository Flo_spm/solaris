/*
 * Code d'exemple pour une photorésistance.
 */
#include <Servo.h>

const int SERVO_PIN = 9;
const int LDR_PIN   = A0;

Servo myservo;  // create servo object to control a servo
int   pos = 0;    // variable to store the servo position

void deplacement_gauche(int deplacement)
{
  pos = pos + deplacement;
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(10);
}

void deplacement_droite(int deplacement)
{
  pos = pos - deplacement;
  myservo.write(pos);              // tell servo to go to position in variable 'pos'
  delay(10);
}

int lecture_capteur()
{
  return analogRead(LDR_PIN);
}

// Fonction setup(), appelée au démarrage de la carte Arduino
void setup() 
{
  // Initialise la communication avec le PC
  Serial.begin(9600);
  
// twelve servo objects can be created on most boards
  myservo.attach(SERVO_PIN);  // attaches the servo on pin 9 to the servo object

  // déplacement gauche et droite
  
}

// Fonction loop(), appelée continuellement en boucle tant que la carte Arduino est alimentée
void loop() 
{
  int valeur_gauche = lecture_capteur();
  Serial.println("valeur gauche : " + String(valeur_gauche));
  deplacement_gauche(10);
  int valeur_droite = lecture_capteur();
  Serial.println("valeur droite : " + String(valeur_droite));
  deplacement_gauche(10);

  // cycle hystérésis à -10 ; +10
  if ( valeur_gauche > valeur_droite - 10 )
    deplacement_gauche(10);
  else if  ( valeur_gauche < valeur_droite  + 10 )
    deplacement_droite(10);

  // Envoi la mesure au PC pour affichage et attends 1000ms
  delay(1000);
}
